/*
 * Dummy HPPA machine.
 *
 *  Copyright (c) 2009 Stuart Brady, Laurent Desnogues
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA  02110-1301 USA
 */

/* This platform is based on IntegratorCP.  */
#include "hw.h"
#include "hw/pc.h"
#include "boards.h"

/* Boot */
#include "sysemu.h"

#define KERNEL_ARGS_ADDR 0x100
#define KERNEL_LOAD_ADDR 0x00010000
#define INITRD_LOAD_ADDR 0x00800000

static uint32_t bootloader[] = {
  0, /* Address of kernel args.  Set by hppa_load_kernel.  */
  0  /* Kernel entry point.  Set by hppa_load_kernel.  */
};

struct hppa_boot_info {
    int ram_size;
    const char *kernel_filename;
    const char *kernel_cmdline;
    const char *initrd_filename;
    target_phys_addr_t loader_start;
    int nb_cpus;
};
static void hppa_load_kernel(CPUState *env, struct hppa_boot_info *info);

static void main_cpu_reset(void *opaque)
{
    CPUState *env = opaque;

    cpu_reset(env);
    if (env->boot_info)
        hppa_load_kernel(env, env->boot_info);
}

/* XXX: tempo */
static void set_kernel_args(struct hppa_boot_info *info,
                            int initrd_size, target_phys_addr_t base)
{
}

/* XXX: tempo */
static
void hppa_load_kernel(CPUState *env, struct hppa_boot_info *info)
{
    int kernel_size;
    int initrd_size;
    int n;
    int is_linux = 0;
    target_ulong entry;
    uint64_t elf_entry;

    /* Load the kernel.  */
    if (!info->kernel_filename) {
        fprintf(stderr, "Kernel image must be specified\n");
        exit(1);
    }

    if (!env->boot_info) {
        if (info->nb_cpus == 0)
            info->nb_cpus = 1;
        env->boot_info = info;
        qemu_register_reset(main_cpu_reset, 0, env);
    }

    /* Assume that raw images are linux kernels, and ELF images are not.  */
    kernel_size = load_elf(info->kernel_filename, 0, &elf_entry, NULL, NULL);
    entry = elf_entry;
    if (kernel_size < 0) {
        kernel_size = load_uimage(info->kernel_filename, &entry, NULL,
                                  &is_linux);
    }
    if (kernel_size < 0) {
        entry = info->loader_start + KERNEL_LOAD_ADDR;
        kernel_size = load_image_targphys(info->kernel_filename, entry,
                                          ram_size - KERNEL_LOAD_ADDR);
        is_linux = 1;
    }
    if (kernel_size < 0) {
        fprintf(stderr, "qemu: could not load kernel '%s'\n",
                info->kernel_filename);
        exit(1);
    }
    if (!is_linux) {
        /* Jump to the entry point.  */
        env->iaoq[0] = entry;
        env->iaoq[1] = entry + 4;
    } else {
        if (info->initrd_filename) {
            initrd_size = load_image_targphys(info->initrd_filename,
                                              info->loader_start
                                              + INITRD_LOAD_ADDR,
                                              ram_size - INITRD_LOAD_ADDR);
            if (initrd_size < 0) {
                fprintf(stderr, "qemu: could not load initrd '%s'\n",
                        info->initrd_filename);
                exit(1);
            }
        } else {
            initrd_size = 0;
        }
        /* XXX:  change these indices.  */
/*         bootloader[1] = info->loader_start + KERNEL_ARGS_ADDR; */
/*         bootloader[2] = entry; */
        for (n = 0; n < sizeof(bootloader) / 4; n++) {
            stl_phys_notdirty(info->loader_start + (n * 4), bootloader[n]);
        }
        set_kernel_args(info, initrd_size, info->loader_start);
    }
}

/* XXX: tempo */
typedef struct {
} dummy_hppa_state;

/* XXX: tempo  */
void irq_info(Monitor *mon)
{
}

/* XXX: tempo  */
void pic_info(Monitor *mon)
{
}

#if 0
/* XXX: tempo */
static uint32_t dummy_hppa_read(void *opaque, target_phys_addr_t offset)
{
    //dummy_hppa_state *s = (dummy_hppa_state *)opaque;

    cpu_abort(cpu_single_env,
               "dummy_hppa_read: Unimplemented offset 0x%x\n", (int)offset);
    return 0;
}

/* XXX: tempo */
static void dummy_hppa_write(void *opaque, target_phys_addr_t offset,
                               uint32_t value)
{
    //dummy_hppa_state *s = (dummy_hppa_state *)opaque;

    cpu_abort(cpu_single_env,
              "dummy_hppa_write: Unimplemented offset 0x%x\n", (int)offset);
}

/* Dummy HPPA board control registers.  */

static CPUReadMemoryFunc *dummy_hppa_readfn[] = {
   dummy_hppa_read,
   dummy_hppa_read,
   dummy_hppa_read
};

static CPUWriteMemoryFunc *dummy_hppa_writefn[] = {
   dummy_hppa_write,
   dummy_hppa_write,
   dummy_hppa_write
};

/*     int iomemtype; */
/*     dummy_hppa_state *s; */

/*     s = (dummy_hppa_state *)qemu_mallocz(sizeof(dummy_hppa_state)); */

/*     iomemtype = cpu_register_io_memory(0, dummy_hppa_readfn, */
/*                                        dummy_hppa_writefn, s); */
/*     cpu_register_physical_memory(0x10000000, 0x00800000, iomemtype); */
/*     cpu_register_physical_memory(0, 0x100000, IO_MEM_RAM); */
#endif

/* Board init.  */

static struct hppa_boot_info hppa_binfo = {
    .loader_start = 0x0,
};

/* XXX: tempo */
static
void dummy_hppa_init(ram_addr_t ram_size,
                       const char *boot_device,
                       const char *kernel_filename, const char *kernel_cmdline,
                       const char *initrd_filename, const char *cpu_model)
{
    CPUState *env;
    ram_addr_t ram_offset;

    env = cpu_init(cpu_model);
    if (!env) {
        fprintf(stderr, "Unable to find CPU definition\n");
        exit(1);
    }

    ram_offset = qemu_ram_alloc(ram_size);
    cpu_register_physical_memory(0, ram_size, ram_offset | IO_MEM_RAM);

    hppa_binfo.ram_size = ram_size;
    hppa_binfo.kernel_filename = kernel_filename;
    hppa_binfo.kernel_cmdline = kernel_cmdline;
    hppa_binfo.initrd_filename = initrd_filename;
    hppa_load_kernel(env, &hppa_binfo);
}

static QEMUMachine dummy_hppa_machine = {
    .name = "dummy",
    .desc = "Dummy HPPA board",
    .init = dummy_hppa_init,
};

static void dummy_hppa_machine_init(void)
{
    qemu_register_machine(&dummy_hppa_machine);
}

machine_init(dummy_hppa_machine_init);
