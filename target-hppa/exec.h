/*
 *  HPPA execution defines
 *
 *  Copyright (c) 2005 Stuart Brady <stuart.brady@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA  02110-1301 USA
 */

#ifndef EXEC_HPPA_H
#define EXEC_HPPA_H

#include "config.h"
#include "dyngen-exec.h"

register struct CPUHPPAState *env asm(AREG0);

#include "cpu.h"
#include "exec-all.h"

static inline void env_to_regs(void)
{
}

static inline void regs_to_env(void)
{
}

static inline int cpu_has_work(CPUState *env)
{
    return env->interrupt_request & (CPU_INTERRUPT_HARD | CPU_INTERRUPT_TIMER);
}

static inline int cpu_halted(CPUState *env) {
    if (!env->halted)
        return 0;
    if (cpu_has_work(env)) {
        env->halted = 0;
        return 0;
    }
    return EXCP_HALTED;
}

void raise_exception(int tt);

#if !defined(CONFIG_USER_ONLY)
#include "softmmu_exec.h"
#endif

#endif
