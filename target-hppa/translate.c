/*
 *  HPPA translation
 * 
 *  Copyright (c) 2005-2009 Stuart Brady <stuart.brady@gmail.com>
 *  Copyright (c) 2007 Randolph Chung <tausq@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA  02110-1301 USA
 */
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>

#include "cpu.h"
#include "exec-all.h"
#include "disas.h"
#include "tcg-op.h"
#include "qemu-common.h"

#include "helpers.h"
#define GEN_HELPER 1
#include "helpers.h"

typedef void (cond_helper)(void);
typedef void (load_helper)(void);
typedef void (store_helper)(void);

#define DYNGEN_OP(op) static inline op;
#include "op_stubs.h"
#undef DYNGEN_OP
#define DYNGEN_OP(op) static inline op {}
#include "op_stubs.h"
#undef DYNGEN_OP

#define DEBUG_DISAS

#define DYNAMIC_PC  1 /* dynamic PC value */
#define JUMP_PC     2 /* dynamic PC value which takes only two values
                         according to jump_pc[T2] */

/* global register indexes */
static TCGv_ptr cpu_env;
static char cpu_reg_names[
    10*4 + 22*5 /* GRs */
];
static TCGv cpu_gr[32];
static TCGv cpu_sr[8];
static TCGv cpu_cr[32];
static TCGv cpu_T[3];

#include "gen-icount.h"

void hppa_translate_init(void)
{
    int i;
    char *p;

    cpu_env = tcg_global_reg_new_ptr(TCG_AREG0, "env");

    p = cpu_reg_names;

    TCGV_UNUSED(cpu_gr[0]);
    for (i = 1; i < 32; i++) {
        sprintf(p, "r%d", i);
        cpu_gr[i] = tcg_global_mem_new(TCG_AREG0,
                                       offsetof(CPUState, gr[i]), p);
        p += (i < 10) ? 3 : 4;
    }
}

typedef struct DisasContext {
    target_ulong iaoq[2];
    target_ulong iasq[2];
    unsigned int is_br:1;
    struct TranslationBlock *tb;
    int mem_idx;
} DisasContext;

static void disas_hppa_insn(DisasContext * dc);

static uint32_t field(uint32_t val, int start, int length)
{
    val >>= start;
    val &= ~(~0 << length);
    return val;
}

static int32_t field_signext(uint32_t val, int start, int length)
{
    val >>= start;
    if (val & (1 << (length - 1)))
        val |= ~0 << length;
    else
        val &= ~(~0 << length);
    return val;
}

static int32_t field_lowsignext(uint32_t val, int start, int length)
{
    if (val & (1 << start)) {
       val >>= start + 1;
       val |= ~0 << (length - 1);
    } else {
       val >>= start + 1;
       val &= ~(~0 << (length - 1));
    }
    return val;
}

static int32_t signext(uint32_t val, int length)
{
    if (val & (1 << (length - 1)))
        val |= ~0 << length;
    return val;
}

static uint32_t assemble_12(uint32_t x, uint32_t y)
{
    return (y << 11) | ((x & 1) << 10) | ((x >> 1) & ~(~0 << 10));
}

static uint32_t assemble_17(uint32_t x, uint32_t y, uint32_t z)
{
    return (z << 16) | (x << 11) | ((y & 1) << 10) | ((y >> 1) & ~(~0 << 10));
}

static uint32_t assemble_21(uint32_t x)
{
    return ((x & 1) << 20) |
           (((x >> 1) & ~(~0 << 11)) << 9) |
           (((x >> 14) & ~(~0 << 2)) << 7) |
           (((x >> 16) & ~(~0 << 5)) << 2) |
           ((x >> 12) & ~(~0 << 2));
}

/* General registers */
static inline void gen_movl_reg_TN(int reg, TCGv tn)
{
    if (reg == 0)
        tcg_gen_movi_tl(tn, 0);
    else
        tcg_gen_mov_tl(tn, cpu_gr[reg]);
}

static inline void gen_movl_TN_reg(int reg, TCGv tn)
{
    if (reg == 0)
        return;
    else
        tcg_gen_mov_tl(cpu_gr[reg], tn);
}

/* Control registers */
static inline void gen_movl_cr_TN(int cr, TCGv tn)
{
    tcg_gen_mov_tl(tn, cpu_cr[cr]);
}

static inline void gen_movl_TN_cr(int cr, TCGv tn)
{
    tcg_gen_mov_tl(cpu_cr[cr], tn);
}

/* Space registers */
static inline void gen_movl_sr_TN(int sr, TCGv tn)
{
    tcg_gen_mov_tl(tn, cpu_sr[sr]);
}

static inline void gen_movl_TN_sr(int sr, TCGv tn)
{
    tcg_gen_mov_tl(cpu_sr[sr], tn);
}

static void gen_goto_tb(DisasContext *dc, int tb_num, 
                               target_ulong pc, target_ulong npc)
{
    TranslationBlock *tb;
    tb = dc->tb;
    if ((tb->pc & TARGET_PAGE_MASK) == (pc & TARGET_PAGE_MASK)) {
        tcg_gen_goto_tb(tb_num);
        gen_op_save_pc(pc, npc);
        tcg_gen_exit_tb((long)tb + tb_num);
    } else {
        gen_op_save_pc(pc, npc);
        tcg_gen_exit_tb(0);
    }
}

static void gen_branch(DisasContext *dc, long tb,
                       target_ulong pc, target_ulong npc)
{
    gen_goto_tb(dc, tb, pc, npc);
}

/* T0 == opd1, T1 == opd2 */
static cond_helper * const gen_cond_add[8] = {
    gen_op_eval_never,
    gen_op_eval_add_eq,
    gen_op_eval_add_slt,
    gen_op_eval_add_slteq,
    gen_op_eval_add_nuv,
    gen_op_eval_add_znv,
    gen_op_eval_add_sv,
    gen_op_eval_add_od,
};

/* T0 == opd1, T1 == opd2 */
static cond_helper * const gen_cond_sub[8] = {
    gen_op_eval_never,
    gen_op_eval_sub_eq,
    gen_op_eval_sub_slt,
    gen_op_eval_sub_slteq,
    gen_op_eval_sub_ult,
    gen_op_eval_sub_ulteq,
    gen_op_eval_sub_sv,
    gen_op_eval_sub_od,
};

/* T0 == opd1, T1 == opd2 */
static cond_helper * const gen_cond_addc[8] = {
    gen_op_eval_never,
    gen_op_eval_addc_eq,
    gen_op_eval_addc_slt,
    gen_op_eval_addc_slteq,
    gen_op_eval_addc_nuv,
    gen_op_eval_addc_znv,
    gen_op_eval_addc_sv,
    gen_op_eval_addc_od,
};

/* T0 == opd1, T1 == opd2 */
static cond_helper * const gen_cond_subb[8] = {
    gen_op_eval_never,
    gen_op_eval_subb_eq,
    gen_op_eval_subb_slt,
    gen_op_eval_subb_slteq,
    gen_op_eval_subb_ult,
    gen_op_eval_subb_ulteq,
    gen_op_eval_subb_sv,
    gen_op_eval_subb_od,
};

/* T0 == res */
static cond_helper * const gen_cond_log[8] = {
    gen_op_eval_never,
    gen_op_eval_log_eq,
    gen_op_eval_log_slt,
    gen_op_eval_log_slteq,
    gen_op_eval_never, /* undefined */
    gen_op_eval_never, /* undefined */
    gen_op_eval_never, /* undefined */
    gen_op_eval_log_od,
};

/* T0 == res */
static cond_helper * const gen_cond_unit[8] = {
    gen_op_eval_never,
    gen_op_eval_never, /* undefined */
    gen_op_eval_unit_sbz,
    gen_op_eval_unit_shz,
    gen_op_eval_unit_sdc,
    gen_op_eval_never, /* undefined */
    gen_op_eval_unit_sbc,
    gen_op_eval_unit_shc,
};

/* T0 == res, T2 == carry */
static cond_helper * const gen_cond_sed[8] = {
    gen_op_eval_never,
    gen_op_eval_log_eq,
    gen_op_eval_log_slt,
    gen_op_eval_log_od,
    gen_op_eval_always,
    gen_op_eval_log_neq,
    gen_op_eval_log_sgteq,
    gen_op_eval_log_ev,
};

static void gen_branch_cond(DisasContext *dc, long tb, target_ulong disp,
                            int n, int f)
{
    int l1;
    target_ulong target;
    target = dc->iaoq[0] + disp + 8;

    l1 = gen_new_label();

    if (f)
        gen_op_jnz_T2_label(l1);
    else
        gen_op_jz_T2_label(l1);

    /* taken branch */
    if (n && ((int32_t)disp >= 0))
        gen_goto_tb(dc, 0, target, target + 4);
    else
        gen_goto_tb(dc, 0, dc->iaoq[1], target);

    gen_set_label(l1);

    /* failed branch */
    if (n && ((int32_t)disp < 0))
        gen_goto_tb(dc, 1, dc->iaoq[1] + 4, dc->iaoq[1] + 8);
    else
        gen_goto_tb(dc, 1, dc->iaoq[1], dc->iaoq[1] + 4);
}

static void gen_nullify_cond(DisasContext *dc, long tb, int f)
{
    int l1;

    l1 = gen_new_label();

    if (f)
        gen_op_jnz_T2_label(l1);
    else
        gen_op_jz_T2_label(l1);

    /* nullify */
    gen_goto_tb(dc, 0, dc->iaoq[1] + 4, dc->iaoq[1] + 8);

    gen_set_label(l1);

    /* don't nullify */
    gen_goto_tb(dc, 1, dc->iaoq[1], dc->iaoq[1] + 4);

    /* FIXME */
    dc->is_br = 1;
}

static void save_state(DisasContext *dc)
{
    /* XXX */
}

static inline void
gen_intermediate_code_internal(CPUState *env, TranslationBlock *tb, 
                               int search_pc)
{
    target_ulong pc_start, last_pc;
    uint16_t *gen_opc_end;
    CPUBreakpoint *bp;
    DisasContext dc1, *dc = &dc1;
    int j, lj = -1;
    int num_insns;
    int max_insns;

    memset(dc, 0, sizeof(DisasContext));
    dc->tb = tb;
    pc_start = tb->pc;
    dc->iaoq[0] = pc_start;
    dc->iaoq[1] = (target_ulong) tb->cs_base;
    last_pc = dc->iaoq[0];
    dc->iasq[0] = dc->iasq[1] = 0; /* FIXME */
    gen_opc_end = gen_opc_buf + OPC_MAX_SIZE;
    num_insns = 0;
    max_insns = tb->cflags & CF_COUNT_MASK;
    if (max_insns == 0)
        max_insns = CF_COUNT_MASK;

    gen_icount_start();
    while (!dc->is_br && gen_opc_ptr < gen_opc_end) {
        if (unlikely(!TAILQ_EMPTY(&env->breakpoints))) {
            TAILQ_FOREACH(bp, &env->breakpoints, entry) {
                if (bp->pc == dc->iaoq[0]) {
                    gen_op_save_pc(dc->iaoq[0], dc->iaoq[1]);
                    gen_op_debug();
                    tcg_gen_exit_tb(0);
                    dc->is_br = 1;
                    goto exit_gen_loop;
                }
            }
        }
        if (search_pc) {
            qemu_log("Search PC...\n");
            j = gen_opc_ptr - gen_opc_buf;
            if (lj < j) {
                lj++;
                while (lj < j)
                    gen_opc_instr_start[lj++] = 0;
                gen_opc_pc[lj] = dc->iaoq[0];
                gen_opc_instr_start[lj] = 1;
                gen_opc_icount[lj] = num_insns;
            }
        }
        if (num_insns + 1 == max_insns && (tb->cflags & CF_LAST_IO))
            gen_io_start();
        last_pc = dc->iaoq[0];
        disas_hppa_insn(dc);
        num_insns++;

        if (dc->is_br)
            break;
        /* if we reach a page boundary, we stop generation so that the
           PC of a TT_TFAULT exception is always in the right page */
        if ((dc->iaoq[0] & (TARGET_PAGE_SIZE - 1)) == 0)
            break;
        /* if single step mode, we generate only one instruction and
           generate an exception */
        if (env->singlestep_enabled || singlestep)
            break;
        if (num_insns >= max_insns)
            break;
    }
    if (tb->cflags & CF_LAST_IO)
        gen_io_end();
    if (env->singlestep_enabled || singlestep) {
        gen_op_save_pc(dc->iaoq[0], dc->iaoq[1]);
        gen_op_debug();
        tcg_gen_exit_tb(0);
    } else if (!dc->is_br) {
        gen_op_save_pc(dc->iaoq[0], dc->iaoq[1]);
        tcg_gen_exit_tb(0);
    }

 exit_gen_loop:
    gen_icount_end(tb, num_insns);
    *gen_opc_ptr = INDEX_op_end;
    if (search_pc) {
        j = gen_opc_ptr - gen_opc_buf;
        lj++;
        while (lj <= j)
            gen_opc_instr_start[lj++] = 0;
        tb->size = 0;
#if 0
        if (loglevel > 0) {
            page_dump(logfile);
        }
#endif
/*
        gen_opc_jump_pc[0] = dc->jump_pc[0];
        gen_opc_jump_pc[1] = dc->jump_pc[1];
*/
    } else {
        tb->size = last_pc + 4 - pc_start;
        tb->icount = num_insns;
    }
#ifdef DEBUG_DISAS
    if (qemu_loglevel_mask(CPU_LOG_TB_IN_ASM)) {
        qemu_log("--------------\n");
        qemu_log("IN: %s\n", lookup_symbol(pc_start));
        log_target_disas(pc_start, last_pc + 4 - pc_start, 0);
        qemu_log("\n");
    }
#endif
}

void gen_intermediate_code(CPUState *env, TranslationBlock *tb)
{
    gen_intermediate_code_internal(env, tb, 0);
}

void gen_intermediate_code_pc(CPUState *env, TranslationBlock *tb)
{
    gen_intermediate_code_internal(env, tb, 1);
}

#define LDST_CMPLT_NONE 0
#define LDST_CMPLT_S    1
#define LDST_CMPLT_SM   2
#define LDST_CMPLT_M    3
#define LDST_CMPLT_MA   4
#define LDST_CMPLT_MB   5

static int get_ldst_cmplt(uint32_t insn)
{
    int indexed_load = (field(insn, 12, 1) == 0);
    if (indexed_load) {
        int u = field(insn, 13, 1);
        int m = field(insn, 5, 1);

        if (u == 0 && m == 0)
            return LDST_CMPLT_NONE;
        else if (u == 0 && m == 1)
            return LDST_CMPLT_M;
        else if (u == 1 && m == 0)
            return LDST_CMPLT_S;
        else if (u == 1 && m == 1)
            return LDST_CMPLT_SM;
    } else {
        int a = field(insn, 13, 1);
        int m = field(insn, 5, 1);
        uint32_t ext4 = field(insn, 6, 4);
        target_long im5;
        if (ext4 <= 7) /* load */
            im5 = field_signext(insn, 16, 5);
        else /* store */
            im5 = field_signext(insn, 0, 5);

        if (m == 0)
            return LDST_CMPLT_NONE;
        else if (a == 0 && m == 1 && im5 != 0)
            return LDST_CMPLT_MA;
        else if (a == 1 && m == 1)
            return LDST_CMPLT_MB;
    }
    return LDST_CMPLT_NONE;
}

static void gen_load(uint32_t insn, int shift, load_helper *op)
{
    int indexed_load = (field(insn, 12, 1) == 0);
    int cmplt = get_ldst_cmplt(insn);
    int b = field(insn, 21, 5);
    int t = field(insn, 0, 5);

    /* load value at address T0 to T1 */

    if (indexed_load) {
        int x = field(insn, 16, 5);
        switch (cmplt) {
        case LDST_CMPLT_S:
        case LDST_CMPLT_SM:
            gen_movl_reg_TN(x, cpu_T[0]);
            tcg_gen_shli_tl(cpu_T[0], cpu_T[0], shift);
            break;
        case LDST_CMPLT_M:
        default:
            gen_movl_reg_TN(x, cpu_T[0]);
            break;
        }
    } else {
        target_long im5 = field_lowsignext(insn, 16, 5);
        tcg_gen_movi_tl(cpu_T[0], im5);
    }

    switch (cmplt) {
    case LDST_CMPLT_MB:
        /* dx in T0 */
        gen_movl_reg_TN(b, cpu_T[1]);
        tcg_gen_mov_tl(cpu_T[2], cpu_T[0]);  /* copy dx to T2 */
        tcg_gen_add_tl(cpu_T[0], cpu_T[1], cpu_T[0]);  /* T0 = GR[b] + dx */
        tcg_gen_add_tl(cpu_T[1], cpu_T[2], cpu_T[1]);  /* GR[b] += dx */
        gen_movl_TN_reg(b, cpu_T[1]);
        break;
    case LDST_CMPLT_MA:
    case LDST_CMPLT_M:
    case LDST_CMPLT_SM:
        /* dx in T0 */
        gen_movl_reg_TN(b, cpu_T[1]);
        tcg_gen_mov_tl(cpu_T[2], cpu_T[1]);
        tcg_gen_add_tl(cpu_T[1], cpu_T[0], cpu_T[1]);  /* GR[b] += dx */
        gen_movl_TN_reg(b, cpu_T[1]);
        tcg_gen_mov_tl(cpu_T[0], cpu_T[2]);  /* T0 = GR[b] */
        break;
    default:
        /* dx in T0 */
        gen_movl_reg_TN(b, cpu_T[1]);
        tcg_gen_add_tl(cpu_T[0], cpu_T[1], cpu_T[0]);  /* T0 = GR[b] + dx */
        break;
    }

    op();
    gen_movl_TN_reg(t, cpu_T[1]);
}

static void gen_store(uint32_t insn, store_helper *op)
{
    target_long im5 = field_lowsignext(insn, 0, 5);
    int r = field(insn, 16, 5);
    int b = field(insn, 21, 5);
    int cmplt = get_ldst_cmplt(insn);

    /* store T1 at address T0 */

    tcg_gen_movi_tl(cpu_T[0], im5);
    switch (cmplt) {
    case LDST_CMPLT_MB:
        gen_movl_reg_TN(b, cpu_T[1]);
        tcg_gen_mov_tl(cpu_T[2], cpu_T[0]); /* T2 = dx */
        tcg_gen_add_tl(cpu_T[0], cpu_T[1], cpu_T[0]); /* offset = GR[b] + dx */
        tcg_gen_add_tl(cpu_T[1], cpu_T[2], cpu_T[1]); /* GR[b] += dx */
        gen_movl_TN_reg(b, cpu_T[1]);
        break;
    case LDST_CMPLT_MA:
        gen_movl_reg_TN(b, cpu_T[1]);
        tcg_gen_mov_tl(cpu_T[2], cpu_T[1]); /* T2 = GR[b] */
        tcg_gen_add_tl(cpu_T[1], cpu_T[0], cpu_T[1]); /* GR[b] += dx */
        gen_movl_TN_reg(b, cpu_T[1]);
        tcg_gen_mov_tl(cpu_T[0], cpu_T[2]); /* offset = GR[b] */
        break;
    default:
        gen_movl_reg_TN(b, cpu_T[1]);
        tcg_gen_add_tl(cpu_T[0], cpu_T[1], cpu_T[0]); /* offset = GR[b] + dx */
        break;
    }
    gen_movl_reg_TN(r, cpu_T[1]);
    op();
}


static void gen_shrpw(uint32_t insn)
{
    int r1 = field(insn, 16, 5);
    int r2 = field(insn, 21, 5);
    int t = field(insn, 0, 5);

    gen_movl_reg_TN(r1, cpu_T[1]);
    gen_movl_reg_TN(r2, cpu_T[2]);
    gen_op_shrpw_cc();
    gen_movl_TN_reg(t, cpu_T[0]);
}

static void gen_extrw(uint32_t insn)
{
    int r = field(insn, 21, 5);
    int t = field(insn, 16, 5);
    int clen = 32 - field(insn, 0, 5);
    int se = field(insn, 10, 1);

    tcg_gen_movi_tl(cpu_T[1], clen);
    gen_movl_reg_TN(r, cpu_T[2]);
    gen_op_extrw_cc();
    gen_movl_TN_reg(t, cpu_T[0]);
}

static void gen_depw(uint32_t insn)
{
    int t = field(insn, 21, 5);
    int r = field(insn, 16, 5);
    int clen = 32 - field(insn, 0, 5);
    int nz = field(insn, 10, 1);

    tcg_gen_movi_tl(cpu_T[1], clen);
    gen_movl_reg_TN(r, cpu_T[2]);
    gen_op_depw_cc();
    gen_movl_TN_reg(t, cpu_T[0]);
}

static void gen_depwi(uint32_t insn)
{
    int t = field(insn, 21, 5);
    target_long im5 = field_signext(insn, 16, 5);
    int clen = 32 - field(insn, 0, 5);
    int nz = field(insn, 10, 1);

    tcg_gen_movi_tl(cpu_T[1], clen);
    tcg_gen_movi_tl(cpu_T[2], im5);
    gen_op_depw_cc();
    gen_movl_TN_reg(t, cpu_T[0]);
}

static void disas_hppa_insn(DisasContext * dc)
{
    uint32_t insn;
    int op;
    int ext6;

    insn = ldl_code(dc->iaoq[0]);
    op = field(insn, 26, 6);

    /* TODO:
     *   traps
     *   signals
     *   spaces
     *   floating point ops
     *   misc ops
     *   refactoring
     *
     * Mostly done:
     *   branches
     *   nullification
     *   conditions
     *   carry
     */

    /* Major Opcodes */
    switch (op) {
    case 0x00: /* System_op */
        {
            int ext8;
            ext8 = field(insn, 5, 8);
            switch (ext8) {
            case 0x00: /* BREAK */
                gen_op_break();
                break;

            case 0x20: /* SYNC/SYNCDMA */
                if (field(insn, 20, 1))
                    gen_op_syncdma();
                else
                    gen_op_sync();
                break;

            case 0x60: /* RFI */
                gen_op_rfi();
                dc->is_br = 1;
                break;

            case 0x65: /* RFI,R (RFIR) */
                gen_op_rfi();
                gen_op_restore_shadow();
                dc->is_br = 1;
                break;

            case 0x6b: /* SSM */
                gen_op_check_priv0();
                gen_op_ssm(field(insn, 16, 7));
                gen_movl_TN_reg(field(insn, 0, 5), cpu_T[0]);
                break;

            case 0x73: /* RSM */
                gen_op_check_priv0();
                gen_op_rsm(field(insn, 16, 7));
                gen_movl_TN_reg(field(insn, 0, 5), cpu_T[0]);
                break;

            case 0xc3: /* MTSM */
                gen_op_check_priv0();
                gen_movl_reg_TN(field(insn, 16, 5), cpu_T[0]);
                gen_op_mtsm();
                break;

            case 0x85: /* LDSID */
                {
                    int s, b, t;
                    s = field(insn, 14, 2);
                    b = field(insn, 21, 5);
                    t = field(insn, 0, 5);
                    break;
                }

            case 0xc1: /* MTSP */
                {
                    int sr = field(insn, 13, 3);
                    int r = field(insn, 16, 5);
                    gen_movl_reg_TN(r, cpu_T[0]);
                    gen_movl_TN_sr(sr, cpu_T[0]);
                    break;
                }

            case 0x25: /* MFSP */
                {
                    int sr = field(insn, 13, 3);
                    int t = field(insn, 0, 5);
                    if (sr >= 3)
                        gen_op_check_priv0();
                    gen_movl_sr_TN(sr, cpu_T[0]);
                    gen_movl_TN_reg(t, cpu_T[0]);
                    break;
                }

#ifdef TARGET_HPPA64
            case 0xa5: /* MFIA */
                break;
#endif

            case 0xc2: /* MTCTL */
                {
                    int t, r;
                    t = field(insn, 21, 5);
                    r = field(insn, 16, 5);
                    if (t >= 1 && t < 7)
                        break;
                    if (t != 11)
                        gen_op_check_priv0();
                    switch (t) {
                    case 0: /* recovery counter */
                        /* TODO: mask 32-bits for 64-bit op */
                    case 14:
                    case 15:
                    case 16:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 8: /* PID0 */
                    case 9: /* PID1 */
                    case 12: /* PID2 */
                    case 13: /* PID3 */
                    /* Undefined in PSW[Q] set */
                    case 17:
                    case 18:
                    case 20:
                    case 21:
                    case 22:
                        gen_movl_reg_TN(r, cpu_T[0]);
                        gen_movl_TN_cr(0, cpu_T[0]);
                        break;

                    case 23:
                        gen_movl_cr_TN(23, cpu_T[0]);
                        gen_movl_reg_TN(r, cpu_T[1]);
                        tcg_gen_andc_tl(cpu_T[0], cpu_T[1], cpu_T[0]);
                        gen_movl_TN_cr(23, cpu_T[0]);
                        break;

                    case 10: /* CCR/SCR */
                    case 11: /* SAR - handled differently on 64-bit */
                        gen_movl_reg_TN(r, cpu_T[0]);
                        tcg_gen_ext8u_tl(cpu_T[0], cpu_T[0]);
                        gen_movl_TN_cr(10, cpu_T[0]);
                        break;
                    }
                    break;
                }

#ifdef TARGET_HPPA64
            case 0xc6: /* MTSARCM */
                break;
#endif

            case 0x45: /* MFCTL/MFCTL,W */
                {
                    int r, t;
                    r = field(insn, 21, 5);
                    t = field(insn, 0, 5);
                    if (r >= 1 && r < 7)
                        break;
                    if (r != 11 && r != 26 && r != 27)
                        gen_op_check_priv0();
                    if (r == 16) /* interval timer */
                        gen_op_check_int_timer_priv();

                    if ((r >= 17 && r <= 22) || (r == 0)) {
                        gen_movl_cr_TN(r, cpu_T[0]);
                        gen_movl_TN_reg(t, cpu_T[0]);
                    } else if (r == 11) { /* SAR */
                        /* Check - may need to mask and shift */
                        gen_movl_cr_TN(r, cpu_T[0]);
                        gen_movl_TN_reg(t, cpu_T[0]);
                    } else if (r >= 8) {
                        gen_movl_cr_TN(r, cpu_T[0]);
                        gen_movl_TN_reg(t, cpu_T[0]);
                    }
                    break;
                }

            default:
                gen_op_undef_insn();
                break;
            }
            break;
        }

    case 0x01: /* Mem_Mgmt */
        {
            int ext5;
            ext5 = field(insn, 0, 5);
            if (!field(insn, 12, 1)) {
                int ext7;
                ext7 = field(insn, 6, 7);
                switch (ext7) {
                case 0x08: /* PITLB */
                case 0x09: /* PITLBE */
                case 0x0a: /* FIC,0A (FIC) */
                case 0x0b: /* FICE */
#ifdef TARGET_HPPA64
                case 0x20: /* IITLBT */
                case 0x18: /* PITLB,L */
#else
                case 0x01: /* IITLBA */
                case 0x00: /* IITLBP */
#endif
                    break;

                default:
                    gen_op_undef_insn();
                    break;
                }
            } else {
                int ext8;
                ext8 = field(insn, 6, 8);

                switch (ext8) {
                case 0x48: /* PDTLB */
                case 0x49: /* PDTLBE */
                case 0x4a: /* FDC (index) */
                case 0x4b: /* FDCE */
                case 0x4e: /* PDC */
                case 0x46: /* PROBE,R (PROBER) */
                case 0xc6: /* PROBEI,R (PROBERI) */
                case 0x47: /* PROBE,W (PROBEW) */
                case 0xc7: /* PROBEI,W (PROBEWI) */
                case 0x4d: /* LPA */
                case 0x4c: /* LCI */
#ifdef TARGET_HPPA64
                case 0x60: /* IDTLBT */
                case 0x58: /* PDTLB,L */
                case 0xca: /* FDC (imm) */
#else
                case 0x41: /* IDTLBA */
                case 0x40: /* IDTLBP */
#endif
                    break;

                default:
                    gen_op_undef_insn();
                    break;
                }
            }
            break;
        }

    case 0x02: /* Arith/Log */
        {
            int t, f, c, r1, r2;
            r2 = field(insn, 21, 5);
            r1 = field(insn, 16, 5);
            c = field(insn, 13, 3);
            f = field(insn, 12, 1);
            ext6 = field(insn, 6, 6);
            t = field(insn, 0, 5);
            gen_movl_reg_TN(r1, cpu_T[0]);
            gen_movl_reg_TN(r2, cpu_T[1]);

            /* Opcode Extensions */
            switch (ext6) {
            case 0x18: /* ADD */
                if (c || f)
                    gen_cond_add[c]();
                gen_helper_add_cc(cpu_T[0], cpu_T[0], cpu_T[1]);
                break;
            case 0x28: /* ADD,L (ADDL) */
                if (c || f)
                    gen_cond_add[c]();
                tcg_gen_add_tl(cpu_T[0], cpu_T[1], cpu_T[0]);
                break;
            case 0x38: /* ADD,TSV (ADDO) */
                gen_op_eval_add_sv();
                /* gen_op_overflow_trap(); */
                if (c || f)
                    gen_cond_add[c]();
                gen_helper_add_cc(cpu_T[0], cpu_T[0], cpu_T[1]);
                break;
            case 0x1c: /* ADD,C (ADDC) */
                if (c || f)
                    gen_cond_addc[c]();
                gen_helper_addc_cc(cpu_T[0], cpu_T[0], cpu_T[1]);
                break;
            case 0x3c: /* ADD,C,TSV (ADDCO) */
                gen_op_eval_addc_sv();
                /* gen_op_overflow_trap(); */
                if (c || f)
                    gen_cond_addc[c]();
                gen_helper_addc_cc(cpu_T[0], cpu_T[0], cpu_T[1]);
                break;
            case 0x19: /* SHLADD (1) (SH1ADD) */
                tcg_gen_shli_tl(cpu_T[0], cpu_T[0], 1);
                if (c || f)
                    gen_cond_add[c](); /* FIXME */
                gen_helper_add_cc(cpu_T[0], cpu_T[0], cpu_T[1]);
                break;
            case 0x29: /* SHLADD,L (1) (SH1ADDL) */
                tcg_gen_shli_tl(cpu_T[0], cpu_T[0], 1);
                if (c || f)
                    gen_cond_add[c]();
                tcg_gen_add_tl(cpu_T[0], cpu_T[1], cpu_T[0]);
                break;
            case 0x39: /* SHLADD,TSV (1) (SH1ADDO) */
                gen_op_eval_add_sv(); /* FIXME */
                /* gen_op_overflow_trap(); */
                tcg_gen_shli_tl(cpu_T[0], cpu_T[0], 1);
                if (c || f)
                    gen_cond_add[c](); /* FIXME */
                gen_helper_add_cc(cpu_T[0], cpu_T[0], cpu_T[1]);
                break;
            case 0x1a: /* SHLADD (2) (SH2ADD) */
                tcg_gen_shli_tl(cpu_T[0], cpu_T[0], 2);
                if (c || f)
                    gen_cond_add[c](); /* FIXME */
                gen_helper_add_cc(cpu_T[0], cpu_T[0], cpu_T[1]);
                break;
            case 0x2a: /* SHLADD,L (2) (SH2ADDL) */
                tcg_gen_shli_tl(cpu_T[0], cpu_T[0], 2);
                if (c || f)
                    gen_cond_add[c]();
                tcg_gen_add_tl(cpu_T[0], cpu_T[1], cpu_T[0]);
                break;
            case 0x3a: /* SHLADD,TSV (2) (SH2ADDO) */
                gen_op_eval_add_sv(); /* FIXME */
                /* gen_op_overflow_trap(); */
                tcg_gen_shli_tl(cpu_T[0], cpu_T[0], 2);
                if (c || f)
                    gen_cond_add[c](); /* FIXME */
                gen_helper_add_cc(cpu_T[0], cpu_T[0], cpu_T[1]);
                break;
            case 0x1b: /* SHLADD (3) (SH3ADD) */
                tcg_gen_shli_tl(cpu_T[0], cpu_T[0], 3);
                if (c || f)
                    gen_cond_add[c](); /* FIXME */
                gen_helper_add_cc(cpu_T[0], cpu_T[0], cpu_T[1]);
                break;
            case 0x2b: /* SHLADD,L (3) (SH3ADDL) */
                tcg_gen_shli_tl(cpu_T[0], cpu_T[0], 3);
                if (c || f)
                    gen_cond_add[c]();
                tcg_gen_add_tl(cpu_T[0], cpu_T[1], cpu_T[0]);
                break;
            case 0x3b: /* SHLADD,TSV (3) (SH3ADDO) */
                gen_op_eval_add_sv(); /* FIXME */
                /* gen_op_overflow_trap(); */
                tcg_gen_shli_tl(cpu_T[0], cpu_T[0], 3);
                if (c || f)
                    gen_cond_add[c](); /* FIXME */
                gen_helper_add_cc(cpu_T[0], cpu_T[0], cpu_T[1]);
                break;
            case 0x10: /* SUB */
                if (c || f)
                    gen_cond_sub[c]();
                gen_op_sub_T1_T0_cc();
                break;
            case 0x30: /* SUB,TSV (SUBO) */
                gen_op_eval_sub_sv();
                /* gen_op_overflow_trap(); */
                if (c || f)
                    gen_cond_sub[c]();
                gen_op_sub_T1_T0_cc();
                break;
            case 0x13: /* SUB,TC (SUBT) */
                if (c || f)
                    gen_cond_sub[c]();
                /* gen_cond_trap(); */
                gen_op_sub_T1_T0_cc();
                break;
            case 0x33: /* SUB,TSV,TC (SUBTO) */
                gen_op_eval_sub_sv();
                /* gen_op_overflow_trap(); */
                if (c || f)
                    gen_cond_sub[c]();
                /* gen_cond_trap(); */
                gen_op_sub_T1_T0_cc();
                break;
            case 0x14: /* SUB,B (SUBB) */
                if (c || f)
                    gen_cond_subb[c]();
                gen_op_subb_T1_T0_cc();
                break;
            case 0x34: /* SUB,B,TSV (SUBBO) */
                gen_op_eval_subb_sv();
                /* gen_op_overflow_trap(); */
                if (c || f)
                    gen_cond_subb[c]();
                gen_op_subb_T1_T0_cc();
                break;
            case 0x11: /* DS */
                /* if (c || f)
                    gen_cond_ds[c](); */
                gen_op_ds_T1_T0();
                break;
            case 0x00: /* ANDCM */
                tcg_gen_andc_tl(cpu_T[0], cpu_T[1], cpu_T[0]);
                if (c || f)
                    gen_cond_log[c]();
                break;
            case 0x08: /* AND */
                tcg_gen_and_tl(cpu_T[0], cpu_T[1], cpu_T[0]);
                if (c || f)
                    gen_cond_log[c]();
                break;
            case 0x09: /* OR */
                tcg_gen_or_tl(cpu_T[0], cpu_T[1], cpu_T[0]);
                if (c || f)
                    gen_cond_log[c]();
                break;
            case 0x0a: /* XOR */
                tcg_gen_xor_tl(cpu_T[0], cpu_T[1], cpu_T[0]);
                if (c || f)
                    gen_cond_log[c]();
                break;
            case 0x0e: /* UXOR */
                gen_op_uxor_T1_T0();
                if (c || f)
                    gen_cond_unit[c]();
                break;
            case 0x22: /* CMPCLR (COMCLR) */
                if (c || f)
                    gen_cond_sub[c]();
                tcg_gen_movi_tl(cpu_T[0], 0);
                break;
            case 0x26: /* UADDCM */
                /* FIXME: 'c' specifies unit size */
                gen_op_uaddcm_T1_T0();
                if (c || f)
                    gen_cond_unit[c]();
                break;
            case 0x27: /* UADDCM,TC (UADDCMT) */
                /* FIXME: 'c' specifies unit size */
                gen_op_uaddcm_T1_T0();
                if (c || f) {
                    gen_cond_unit[c]();
                    /* gen_cond_trap(); */
                }
                break;
            case 0x2e: /* DCOR */
                if (r1 != 0)
                    gen_op_undef_insn();
                else {
                    tcg_gen_mov_tl(cpu_T[0], cpu_T[1]);
                    gen_op_dcor_T0();
                }
                break;
            case 0x2f: /* DCOR,I (IDCOR) */
                if (r1 != 0)
                    gen_op_undef_insn();
                else {
                    tcg_gen_mov_tl(cpu_T[0], cpu_T[1]);
                    gen_op_idcor_T0();
                }
                break;
#ifdef TARGET_HPPA64
            /* Also included in MAX-1 (PA-7100LC and PA-7300LC) */
            case 0x0f: /* HADD */
            case 0x0d: /* HADD,SS */
            case 0x0c: /* HADD,US */
            case 0x07: /* HSUB */
            case 0x05: /* HSUB,SS */
            case 0x04: /* HSUB,US */
            case 0x0b: /* HAVG (HAVE) */
            case 0x1d: /* HSHLADD (1) (HSL1ADD) */
            case 0x1e: /* HSHLADD (2) (HSL2ADD) */
            case 0x1f: /* HSHLADD (3) (HSL3ADD) */
            case 0x15: /* HSHRADD (1) (HSR1ADD) */
            case 0x16: /* HSHRADD (2) (HSR2ADD) */
            case 0x17: /* HSHRADD (3) (HSR3ADD) */
                break;
#endif
            default: /* Undefined Instruction */
                gen_op_undef_insn();
                break;
            }
            gen_movl_TN_reg(t, cpu_T[0]);
            if (c || f)
                gen_nullify_cond(dc, (long)dc->tb, f);
            break;
        }

    case 0x03: /* Index_Mem */
        {
            int ext4 = field(insn, 6, 4);
            switch (ext4) {
            /* XXX: gen_op_*_raw only works for user-mode emulation
             *      we really need gen_load and gen_store to be macros
             *      to allow _phys and _virtual to be used
             */
            case 0x00: /* LDB (index/short) (LDBX/LDBS) */
                gen_load(insn, 0, gen_op_ldb_raw);
                break;
            case 0x01: /* LDH (index/short) (LDHX/LDHS) */
                gen_load(insn, 1, gen_op_ldh_raw);
                break;
            case 0x02: /* LDW (index/short) (LDWX/LDWS) */
                gen_load(insn, 2, gen_op_ldw_raw);
                break;
            case 0x06: /* LDWA (index/short) (LDWAX/LDWAS) */
                gen_op_check_priv0();
                gen_load(insn, 2, gen_op_ldw_phys);
                break;
            case 0x07: /* LDCW (index/short) (LDCWX/LDCWS) */
                gen_load(insn, 2, gen_op_ldcw_raw);
                break;
            case 0x08: /* STB (short) (STBS) */
                gen_store(insn, gen_op_stb_raw);
                break;
            case 0x09: /* STH (short) (STHS) */
                gen_store(insn, gen_op_sth_raw);
                break;
            case 0x0a: /* STW (short) (STWS) */
                gen_store(insn, gen_op_stw_raw);
                break;
            case 0x0c: /* STBY (short) (STBYS) */
                /* XXX */
                break;
            case 0x0e: /* STWA (short) (STWAS) */
                gen_store(insn, gen_op_stw_phys);
                break;

#ifdef TARGET_HPPA64
            case 0x03: /* LDD (index/short) */
            case 0x04: /* LDDA (index/short) */
            case 0x05: /* LDCD (index/short) */
            case 0x0b: /* STD (short) */
            case 0x0d: /* STDBY (short) */
            case 0x0f: /* STDA (short) */
                /* XXX - pa20 */
                break;
#endif
            }
            break;
        }

    case 0x04: /* SPOPn */
        /* SPOP0 */
        /* SPOP1 */
        /* SPOP2 */
        /* SPOP3 */
        break;

    case 0x05: /* DIAG */
    case 0x06: /* FMPYADD */
        break;

    case 0x08: /* LDIL */
        {
            int t;
            target_ulong im21;
            t = field(insn, 21, 5);
            if (t) {
                im21 = assemble_21(field(insn, 0, 21)) << (32 - 21);
                tcg_gen_movi_tl(cpu_T[0], im21);
                gen_movl_TN_reg(t, cpu_T[0]);
            }
            break;
        }

    case 0x09: /* Copr_w */
        if (!field(insn, 12, 1))
            if (!field(insn, 9, 1))
                /* CLDW (index) (CLDWX) */ {}
            else
                /* CSTW (index) (CSTWX) */ {}
        else
            if (!field(insn, 9, 1))
                /* CLDW (short) (CLDWS) */ {}
            else
                /* CSTW (short) (CSTWS) */ {}
        break;

    case 0x0a: /* ADDIL */
        {
            int r;
            target_ulong im21;
            r = field(insn, 21, 5);
            im21 = assemble_21(field(insn, 0, 21)) << (32 - 21);
            gen_movl_reg_TN(r, cpu_T[1]);
            tcg_gen_movi_tl(cpu_T[0], im21);
            tcg_gen_add_tl(cpu_T[0], cpu_T[1], cpu_T[0]);
            gen_movl_TN_reg(1, cpu_T[0]);
            break;
        }

    case 0x0b: /* Copr_dw */
        if (!field(insn, 12, 1))
            if (!field(insn, 9, 1))
                /* CLDD (index) (CLDDX) */ {}
            else
                /* CSTD (index) (CLTDX) */ {}
        else
            if (!field(insn, 9, 1))
                /* CLDD (short) (CLDDS) */ {}
            else
                /* CSTD (short) (CSTDS) */ {}
        break;

    case 0x0c: /* COPR */
        break;

    case 0x0d: /* LDO - Load Offset */
        {
            int b, t;
            target_long im14;
            b = field(insn, 21, 5);
            t = field(insn, 16, 5);
            im14 = field_lowsignext(insn, 0, 14);
            gen_movl_reg_TN(b, cpu_T[0]);
            tcg_gen_movi_tl(cpu_T[1], im14);
            tcg_gen_add_tl(cpu_T[0], cpu_T[1], cpu_T[0]);
            gen_movl_TN_reg(t, cpu_T[0]);
            break;
        }

    case 0x0e: /* Float */
    case 0x0f: /* Product Specific */
        break;

    case 0x10: /* LDB */
    case 0x11: /* LDH */
    case 0x12: /* LDW */
        {
            int b, t, s;
            target_long im14;
            b = field(insn, 21, 5);
            t = field(insn, 16, 5);
            s = field(insn, 14, 2);
            im14 = field_lowsignext(insn, 0, 14);
            gen_movl_reg_TN(b, cpu_T[0]);
            tcg_gen_movi_tl(cpu_T[1], s);
            /* gen_op_space_sel_T0_T1(); */
            tcg_gen_movi_tl(cpu_T[1], im14);
            tcg_gen_add_tl(cpu_T[0], cpu_T[1], cpu_T[0]);
            switch (op) {
                case 0x10: /* LDB */
                    tcg_gen_qemu_ld8u(cpu_T[1], cpu_T[0], dc->mem_idx);
                    break;
                case 0x11: /* LDH */
                    tcg_gen_qemu_ld16u(cpu_T[1], cpu_T[0], dc->mem_idx);
                    break;
                case 0x12: /* LDW */
                    tcg_gen_qemu_ld32u(cpu_T[1], cpu_T[0], dc->mem_idx);
                    break;
            }
            gen_movl_TN_reg(t, cpu_T[1]);
            break;
        }

    case 0x13: /* LDW,M (LDWM) post-incr/pre-decr */
        {
            int b, t, s;
            target_long im14;
            b = field(insn, 21, 5);
            t = field(insn, 16, 5);
            s = field(insn, 14, 2);
            im14 = field_lowsignext(insn, 0, 14);
            gen_movl_reg_TN(b, cpu_T[0]);
            /* tcg_gen_movi_tl(cpu_T[1], s); */
            /* gen_op_space_sel_T0_T1(); */
            /* XXX: check this */
            if (im14 & (1 << 31)) {
                tcg_gen_movi_tl(cpu_T[1], im14);
                tcg_gen_add_tl(cpu_T[0], cpu_T[1], cpu_T[0]);
            }
            tcg_gen_qemu_ld32u(cpu_T[1], cpu_T[0], dc->mem_idx);
            gen_movl_TN_reg(t, cpu_T[1]);
            if (!(im14 & (1 << 31))) {
                tcg_gen_movi_tl(cpu_T[1], im14);
                tcg_gen_add_tl(cpu_T[0], cpu_T[1], cpu_T[0]);
            }
            gen_movl_TN_reg(b, cpu_T[0]);
            break;
        }

#ifdef TARGET_HPPA64
    case 0x14: /* Load_dw */
        /* LDD/FLDD (long) */
        break;

    case 0x16: /* FLDW,M */
    case 0x17: /* LDW,M pre-incr/post-decr */
        break;
#endif

    case 0x18: /* STB */
    case 0x19: /* STH */
    case 0x1a: /* STW */
        {
            int b, r, s;
            target_long im14;
            b = field(insn, 21, 5);
            r = field(insn, 16, 5);
            s = field(insn, 14, 2);
            im14 = field_lowsignext(insn, 0, 14);
            gen_movl_reg_TN(b, cpu_T[0]);
            /* tcg_gen_movi_tl(cpu_T[1], s); */
            /* gen_op_space_sel_T0_T1(); */
            tcg_gen_movi_tl(cpu_T[1], im14);
            tcg_gen_add_tl(cpu_T[0], cpu_T[1], cpu_T[0]);
            gen_movl_reg_TN(r, cpu_T[1]);
            switch (op) {
            case 0x18: /* STB */
                tcg_gen_qemu_st8(cpu_T[1], cpu_T[0], dc->mem_idx);
                break;
            case 0x19: /* STH */
                tcg_gen_qemu_st16(cpu_T[1], cpu_T[0], dc->mem_idx);
                break;
            case 0x1a: /* STW */
                tcg_gen_qemu_st32(cpu_T[1], cpu_T[0], dc->mem_idx);
                break;
            }
            break;
        }

    case 0x1b: /* STW,M (STWM) post-incr/pre-decr */
        {
            int b, r, s;
            target_long im14;
            b = field(insn, 21, 5);
            r = field(insn, 16, 5);
            s = field(insn, 14, 2);
            im14 = field_lowsignext(insn, 0, 14);
            gen_movl_reg_TN(b, cpu_T[0]);
            /* tcg_gen_movi_tl(cpu_T[1], s); */
            /* gen_op_space_sel_T0_T1(); */
            /* XXX: check this */
            if (im14 & (1 << 31)) {
                tcg_gen_movi_tl(cpu_T[1], im14);
                tcg_gen_add_tl(cpu_T[0], cpu_T[1], cpu_T[0]);
            }
            gen_movl_reg_TN(r, cpu_T[1]);
            tcg_gen_qemu_st32(cpu_T[1], cpu_T[0], dc->mem_idx);
            if (!(im14 & (1 << 31))) {
                tcg_gen_movi_tl(cpu_T[1], im14);
                tcg_gen_add_tl(cpu_T[0], cpu_T[1], cpu_T[0]);
            }
            gen_movl_TN_reg(b, cpu_T[0]);
            break;
        }

#ifdef TARGET_HPPA64
    case 0x1c: /* Store_dw */
        /* STD/FSTD (long) */
        break;

    case 0x1e: /* FSTW,M */
    case 0x1f: /* STW,M pre-incr/post-decr */
        break;
#endif

    case 0x20: /* CMPB (true) (COMBT) */
    case 0x21: /* CMPIB (true) (COMIBT) */
    case 0x22: /* CMPB (false) (COMBF) */
    case 0x23: /* CMPIB (false) (COMIBF) */
        {
            int f, c, w1, n, w;
            target_long disp;
            switch (op) {
            case 0x20: /* CMPB (true) (COMBT) */
            case 0x22: /* CMPB (false) (COMBF) */
                {
                    int r1, r2;
                    r2 = field(insn, 21, 5);
                    r1 = field(insn, 16, 5);
                    gen_movl_reg_TN(r1, cpu_T[0]);
                    gen_movl_reg_TN(r2, cpu_T[1]);
                    break;
                }
            case 0x21: /* CMPIB (true) (COMIBT) */
            case 0x23: /* CMPIB (false) (COMIBF) */
                {
                    int r;
                    target_long im5;
                    r = field(insn, 21, 5);
                    im5 = field_lowsignext(insn, 16, 5);
                    tcg_gen_movi_tl(cpu_T[0], im5);
                    gen_movl_reg_TN(r, cpu_T[1]);
                    break;
                }
            }
            f = field(insn, 27, 1);
            c = field(insn, 13, 3);
            w1 = field(insn, 2, 11);
            n = field(insn, 1, 1);
            w = field(insn, 0, 1);
            disp = signext(assemble_12(w1, w), 12) << 2;

            gen_cond_sub[c]();
            gen_branch_cond(dc, (long)dc->tb, disp, n, f);

            /* FIXME */
            dc->is_br = 1;
            break;
        }

    case 0x24: /* CMPICLR (COMICLR) */
        {
            int r, t, c, f;
            target_long im11;
            if (field(insn, 11, 1))
                gen_op_undef_insn();
            else {
                r = field(insn, 21, 5);
                t = field(insn, 16, 5);
                c = field(insn, 13, 3);
                f = field(insn, 12, 1);
                im11 = field_lowsignext(insn, 0, 11);
                gen_movl_reg_TN(r, cpu_T[0]);
                tcg_gen_movi_tl(cpu_T[1], im11);
                if (c || f)
                    gen_cond_sub[c]();
                tcg_gen_movi_tl(cpu_T[0], 0);
                gen_movl_TN_reg(t, cpu_T[0]);
                if (c || f)
                    gen_nullify_cond(dc, (long)dc->tb, f);
            }
            break;
        }

    case 0x25: /* SUBI / SUBI,TSV (SUBIO) */
        {
            int r, t, c, f, o;
            target_long im11;
            r = field(insn, 21, 5);
            t = field(insn, 16, 5);
            c = field(insn, 13, 3);
            f = field(insn, 12, 1);
            o = field(insn, 11, 1);
            im11 = field_lowsignext(insn, 0, 11);
            gen_movl_reg_TN(r, cpu_T[0]);
            if (o) {
                gen_op_eval_sub_sv();
                /* gen_op_overflow_trap(); */
            }
            if (c || f)
                gen_cond_sub[c]();
            gen_op_sub_T1_T0_cc();
            if (c || f)
                gen_nullify_cond(dc, (long)dc->tb, f);
            break;
        }

    case 0x26: /* FMPYSUB */
        /* TODO */
        break;

#ifdef TARGET_HPPA64
    case 0x27: /* CMPB (dw true) */
        break;
#endif

    case 0x28: /* ADDB (true) (ADDBT) */
    case 0x29: /* ADDIB (true) (ADDIBT) */
    case 0x2a: /* ADDB (false) (ADDBF) */
    case 0x2b: /* ADDIB (false) (ADDIBF) */
        {
            int f, c, w1, n, w;
            target_long disp;
            switch (op) {
            case 0x28: /* ADDB (true) (ADDBT) */
            case 0x2a: /* ADDB (false) (ADDBF) */
                {
                    int r1, r2;
                    r2 = field(insn, 21, 5);
                    r1 = field(insn, 16, 5);
                    gen_movl_reg_TN(r1, cpu_T[0]);
                    gen_movl_reg_TN(r2, cpu_T[1]);
                    break;
                }
            case 0x29: /* ADDIB (true) (ADDIBT) */
            case 0x2b: /* ADDIB (false) (ADDIBF) */
                {
                    int r;
                    target_long im5;
                    r = field(insn, 21, 5);
                    im5 = field_lowsignext(insn, 16, 5);
                    tcg_gen_movi_tl(cpu_T[0], im5);
                    gen_movl_reg_TN(r, cpu_T[1]);
                    break;
                }
            }
            f = field(insn, 27, 1);
            c = field(insn, 13, 3);
            w1 = field(insn, 2, 11);
            n = field(insn, 1, 1);
            w = field(insn, 0, 1);
            disp = signext(assemble_12(w1, w), 12) << 2;

            gen_cond_add[c]();
            gen_branch_cond(dc, (long)dc->tb, disp, n, f);

            /* FIXME */
            dc->is_br = 1;
            break;
        }

    case 0x2c: /* ADDI,TC (ADDIT) / ADDI,TSV,TC (ADDITO) */
    case 0x2d: /* ADDI / ADDI,TSV (ADDIO) */
        {
            int r, t, c, f, o;
            target_long im11;
            r = field(insn, 21, 5);
            t = field(insn, 16, 5);
            c = field(insn, 13, 3);
            f = field(insn, 12, 1);
            o = field(insn, 11, 1);
            im11 = field_lowsignext(insn, 0, 11);
            gen_movl_reg_TN(r, cpu_T[0]);
            tcg_gen_movi_tl(cpu_T[1], im11);
            if (o) {
                gen_op_eval_add_sv();
                /* gen_op_overflow_trap(); */
            }
            if (c || f) {
                gen_cond_add[c]();
                if (op == 0x2c) /* ADDI,TC (ADDIT) / ADDI,TSV,TC (ADDITO) */
                    ; /* gen_cond_trap(); */
            }
            gen_helper_add_cc(cpu_T[0], cpu_T[0], cpu_T[1]);
            if ((c || f) && (op == 0x2d)) /* ADDI / ADDI,TSV (ADDIO) */
                gen_nullify_cond(dc, (long)dc->tb, f);
            gen_movl_TN_reg(t, cpu_T[0]);
            break;
        }

#ifdef TARGET_HPPA64
    case 0x2e: /* Fp_fused */
    case 0x2f: /* CMPB (dw false) */
        break;
#endif

    case 0x30: /* BB (sar) (BVB) */
    case 0x31: /* BB */
    case 0x32: /* MOVB */
    case 0x33: /* MOVIB */
        /* disp = signext(assemble_12(w1, w), 12) << 2; */
        /* FIXME */
        dc->is_br = 1;
        break;

    case 0x34: /* Shift/Extract */
        {
            int ext3;
            ext3 = field(insn, 10, 3);
            switch (ext3) {
            case 0: /* VSHD = SHRPW with SAR */
                gen_movl_cr_TN(11, cpu_T[0]);
                gen_shrpw(insn);
                break;
            case 2: /* SHD = SHRPW */
                {
                    int sa = 31 - field(insn, 5, 5);
                    tcg_gen_movi_tl(cpu_T[0], sa);
                    gen_shrpw(insn);
                    break;
                }
            case 4: /* VEXTRU = EXTRW,U with SAR */
            case 5: /* VEXTRS = EXTRW,S with SAR */
                gen_movl_cr_TN(11, cpu_T[0]);
                gen_extrw(insn);
                break;
            case 6: /* EXTRU = EXTRW,U */
            case 7: /* EXTRS = EXTRW,S */
                {
                    int pos = field(insn, 5, 5);
                    tcg_gen_movi_tl(cpu_T[0], pos);
                    gen_extrw(insn);
                    break;
                }
            }
            break;
        }

    case 0x35: /* Deposit */
        {
            int ext3;
            ext3 = field(insn, 10, 3);
            switch (ext3) {
            case 0: /* VZDEP = DEPW,Z with SAR */
            case 1: /* VDEP = DEPW with SAR */
                gen_movl_cr_TN(11, cpu_T[0]);
                gen_depw(insn);
                break;
            case 2: /* ZDEP = DEPW,Z */
            case 3: /* DEP = DEPW */
                {
                    int cpos = field(insn, 5, 5);
                    tcg_gen_movi_tl(cpu_T[0], cpos);
                    gen_depw(insn);
                    break;
                }
            case 4: /* VZDEPI = DEPW,Z */
            case 5: /* VDEPI = DEPW,Z */
                gen_movl_cr_TN(11, cpu_T[0]);
                gen_depwi(insn);
                break;
            case 6: /* ZDEPI = DEPWI,Z */
            case 7: /* DEPI = DEPWI */
                {
                    int cpos = field(insn, 5, 5);
                    tcg_gen_movi_tl(cpu_T[0], cpos);
                    gen_depwi(insn);
                    break;
                }
            }
            break;
        }

#ifdef TARGET_HPPA64
    case 0x36: /* EXTRD */
        break;
#endif

    case 0x38: /* BE */
    case 0x39: /* BE,L (BLE) */
        {
            int b, w1, s, w2, n, w;
            target_long disp;
            b = field(insn, 21, 5);
            w1 = field(insn, 16, 5);
            s = field(insn, 13, 3);
            w2 = field(insn, 2, 11);
            n = field(insn, 1, 1);
            w = field(insn, 0, 1);
            disp = signext(assemble_17(w1,w2,w),17) << 2;
            /*  */

            /* FIXME */
            dc->is_br = 1;
            break;
        }

    case 0x3a: /* Branch */
        {
            int t, w, w1, ext3, w2, n;
            target_long disp;
            ext3 = field(insn, 13, 3);
            t = field(insn, 21, 5);
            w1 = field(insn, 16, 5);
            w2 = field(insn, 2, 11);
            n = field(insn, 1, 1);
            w = field(insn, 0, 1);
            disp = signext(assemble_17(w1,w2,w),17) << 2;
            switch (ext3) {
            case 0: /* B,L (BL) */
                /* TODO: dc->iaoq[1] + 4 into t */
                if (n) {
                    gen_branch(dc, 0, dc->iaoq[0] + disp + 8, dc->iaoq[0] + disp + 12);
                } else {
                    gen_branch(dc, 0, dc->iaoq[1], dc->iaoq[0] + disp + 8);
                }
                break;
            case 2: /* BLR */
                /* if w == 0 ( ill_insn ) */
            case 6: /* BV */
            case 1: /* B,GATE (GATE) */
                break;
            }

            /* FIXME */
            dc->is_br = 1;
            break;
        }

#ifdef TARGET_HPPA64
    case 0x3b: /* CMPIB (dw) */
        break;

    case 0x3c: /* DEPD */
    case 0x3d: /* DEPI */
        break;

    case 0x3e: /* Multimedia (MAX-2) */
        /* PERMH */
        /* HSHL */
        /* HSHR,U */
        /* HSHR,S */
        /* MIXW,L */
        /* MIXW,R */
        /* MIXH,L */
        /* MIXH,R */
        break;
#endif

    default: /* Illegal Instruction */
        gen_op_ill_insn();
        break;
    }

    dc->iaoq[0] = dc->iaoq[1];
    dc->iaoq[1] = dc->iaoq[1] + 4;
}

CPUHPPAState *cpu_hppa_init(const char *cpu_model)
{
    CPUHPPAState *env;

    env = qemu_mallocz(sizeof(CPUHPPAState));
    cpu_exec_init(env);
    hppa_translate_init();
    tlb_flush(env, 1);
    qemu_init_vcpu(env);
    return env;
}

void cpu_dump_state(CPUState *env, FILE *f, 
                    int (*cpu_fprintf)(FILE *f, const char *fmt, ...),
                    int flags)
{
    int i;

    cpu_fprintf(f, "PSW = %08X\n", env->psw);
    for (i=0; i<31; i++) {
        cpu_fprintf(f, "R%02d=%08x", i, env->gr[i]);
        if ((i % 4) == 3)
            cpu_fprintf(f, "\n");
        else
            cpu_fprintf(f, " ");
    }

    for (i=0; i<31; i++) {
        cpu_fprintf(f, "CR%02d=%08x", i, env->cr[i]);
        if ((i % 4) == 3)
            cpu_fprintf(f, "\n");
        else
            cpu_fprintf(f, " ");
    }

    cpu_fprintf(f, "IAOQ = %08X %08X\tIASQ = %08X %08X\n",
                env->iaoq[0], env->iaoq[1], env->iasq[0], env->iasq[1]);
}

void gen_pc_load(CPUState *env, TranslationBlock *tb,
                 unsigned long searched_pc, int pc_pos, void *puc)
{
    env->iaoq[0] = gen_opc_pc[pc_pos];
    env->iaoq[1] = gen_opc_npc[pc_pos];
}
