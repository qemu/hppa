/*
 * HPPA helpers
 *
 *  Copyright (c) 2005-2009 Stuart Brady <stuart.brady@gmail.com>
 *  Copyright (c) 2007 Randolph Chung <tausq@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA  02110-1301 USA
 */

#include <string.h>

#include "exec.h"

/* XXX: tempo */
void cpu_reset(CPUHPPAState *env)
{
    if (qemu_loglevel_mask(CPU_LOG_RESET)) {
        qemu_log("CPU Reset (CPU %d)\n", env->cpu_index);
        log_cpu_state(env, 0);
    }

    memset(env, 0, offsetof(CPUHPPAState, breakpoints));
    env->iaoq[0] = 0;
    env->iaoq[1] = 4;
    tlb_flush(env, 1);
}

void raise_exception(int tt)
{
    env->cr[18] = env->iaoq[0];
    env->iiaoq_back = env->iaoq[1];
    env->cr[17] = env->iasq[0];
    env->iiasq_back = env->iasq[1];
    env->cr[19] = ldl_code(env->iaoq[0]);
    env->cr[20] = 0; /* isr */
    env->cr[21] = 0; /* ior */
    env->cr[22] = env->psw;
    env->exception_index = tt;
    cpu_loop_exit();
}

int cpu_hppa_handle_mmu_fault (CPUState *env, target_ulong address, int rw,
                               int is_user, int is_softmmu)
{
    return 1;
}

target_phys_addr_t cpu_get_phys_page_debug(CPUState *env, target_ulong addr)
{
    return addr;
}


#if defined (CONFIG_USER_ONLY)
void do_interrupt (CPUState *env)
{
    env->exception_index = -1;
}
#else
void do_interrupt (CPUState *env)
{
}
#endif /* !CONFIG_USER_ONLY */
