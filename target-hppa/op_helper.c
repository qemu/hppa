/*
 *  HPPA emulation helpers
 *
 *  Copyright (c) 2005-2007 Stuart Brady <stuart.brady@gmail.com>
 *  Copyright (c) 2007 Randolph Chung <tausq@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA  02110-1301 USA
 */

#include "exec.h"
#include "helpers.h"

#if !defined(CONFIG_USER_ONLY)

#define MMUSUFFIX _mmu

#define SHIFT 0
#include "softmmu_template.h"

#define SHIFT 1
#include "softmmu_template.h"

#define SHIFT 2
#include "softmmu_template.h"

#define SHIFT 3
#include "softmmu_template.h"

/* XXX: tempo */
void tlb_fill(target_ulong addr, int is_write, int mmu_idx, void *retaddr)
{
    CPUState *saved_env;
    int ret;
    target_phys_addr_t phys_addr;
    int prot;

    /* XXX: hack to restore env in all cases, even if not called from
       generated code */
    saved_env = env;
    env = cpu_single_env;

    /* XXX: flat mapping for the moment...  */
    phys_addr = addr;
    prot = PAGE_READ | PAGE_WRITE | PAGE_EXEC;
    ret = tlb_set_page(env, addr & TARGET_PAGE_MASK, phys_addr, prot, mmu_idx, 1);

    env = saved_env;
}

#endif

#if 0
void OPPROTO op_ldcw_raw(void)
{
    /* FIXME - should be atomic */
    T1 = ldl_raw((void *)T0);
    stl_raw((void *)T0, 0);
}

void OPPROTO op_ldw_phys(void)
{
    /* FIXME - T0 contains a physical address */
    T1 = ldl_raw((void *)T0);
}

void OPPROTO op_stw_phys(void)
{
    /* FIXME - T0 contains a physical address */
    stl_raw((void *)T0, T1);
}

void OPPROTO op_debug(void)
{
    raise_exception(EXCP_DEBUG);
}

/* System operations */
void OPPROTO op_break(void)
{
    raise_exception(EXCP_BREAK);
}

void OPPROTO op_check_priv0(void)
{
    if (env->priv_level != 0)
        raise_exception(EXCP_PRIVOP);
}

void OPPROTO op_check_int_timer_priv(void)
{
    /* secure interval timer */
    if ((env->psw & PSW_S) && env->priv_level != 0)
        raise_exception(EXCP_PRIVOP);
}

void OPPROTO op_sync(void)
{
}

void OPPROTO op_syncdma(void)
{
}

void OPPROTO op_rfi(void)
{
    env->psw = env->cr[22]; /* ipsw */
    env->iaoq[0] = env->cr[18]; /* iiaoq */
    env->iaoq[1] = env->iiaoq_back;
    env->iasq[0] = env->cr[17]; /* iiasq */
    env->iasq[1] = env->iiasq_back;
}

void OPPROTO op_restore_shadow(void)
{
    /* restore shadow registers */
    env->gr[1] = env->shr[0];
    env->gr[8] = env->shr[1];
    env->gr[9] = env->shr[2];
    env->gr[16] = env->shr[3];
    env->gr[17] = env->shr[4];
    env->gr[24] = env->shr[5];
    env->gr[25] = env->shr[6];
}

void OPPROTO op_ssm(void)
{
    T0 = env->psw & (PSW_G | PSW_F | PSW_R | PSW_Q | PSW_P | PSW_D | PSW_I);

    if (PARAM1 & (1 << 0))
      env->psw |= PSW_I;
    if (PARAM1 & (1 << 1))
      env->psw |= PSW_D;
    if (PARAM1 & (1 << 2))
      env->psw |= PSW_P;
    if (PARAM1 & (1 << 4))
      env->psw |= PSW_R;
    if (PARAM1 & (1 << 5))
      env->psw |= PSW_F;
    if (PARAM1 & (1 << 6))
      env->psw |= PSW_G;
}

void OPPROTO op_rsm(void)
{
    T0 = env->psw & (PSW_G | PSW_F | PSW_R | PSW_Q | PSW_P | PSW_D | PSW_I);

    if (PARAM1 & (1 << 0))
      env->psw &= ~PSW_I;
    if (PARAM1 & (1 << 1))
      env->psw &= ~PSW_D;
    if (PARAM1 & (1 << 2))
      env->psw &= ~PSW_P;
    if (PARAM1 & (1 << 4))
      env->psw &= ~PSW_R;
    if (PARAM1 & (1 << 5))
      env->psw &= ~PSW_F;
    if (PARAM1 & (1 << 6))
      env->psw &= ~PSW_G;
}

void OPPROTO op_mtsm(void)
{
    env->psw = env->psw & ~(PSW_G | PSW_F | PSW_R | PSW_Q | PSW_P | PSW_D | PSW_I);
    env->psw |= T0 & (PSW_G | PSW_F | PSW_R | PSW_Q | PSW_P | PSW_D | PSW_I);
}
#endif

/* computation instructions... page 169 PA1.1 specification */

/* TODO: lazy PSW */
target_ulong helper_add_cc(target_ulong t0, target_ulong t1)
{
    target_ulong carry;
    target_ulong res;

    res = t0 + t1;

    /* calculate carry flags */
    carry = (t0 & t1) | ((t0 | t1) & ~res);
    /* extract the MSB from each nibble */
                          /* axxxbxxxcxxxdxxxexxxfxxxgxxxhxxx */
    carry >>= 3;          /* 000axxxbxxxcxxxdxxxexxxfxxxgxxxh */
    carry &= 0x11111111;  /* 000a000b000c000d000e000f000g000h */
    carry |= carry >> 3;  /* 000a00ab00bc00cd00de00ef00fg00gh */
    carry &= 0x03030303;  /* 000000ab000000cd000000ef000000gh */
    carry |= carry >> 6;  /* 000000ab0000abcd000000ef0000efgh */
    carry &= 0x000f000f;  /* 000000000000abcd000000000000efgh */
    carry |= carry >> 12; /* 000000000000abcd00000000abcdefgh */
    carry &= 0x000000ff;  /* 000000000000000000000000abcdefgh */

    env->psw &= ~PSW_CB;
    env->psw |= carry << PSW_CB_SHIFT;

    return res;
}

/* TODO: lazy PSW */
target_ulong helper_addc_cc(target_ulong t0, target_ulong t1)
{
    target_ulong carry;
    target_ulong res;

    res = t0 + t1;

    /* add the carry */
    if (env->psw & PSW_CB7) {
        res += 1;
    }

    /* calculate carry flags */
    carry = (t0 & t1) | ((t0 | t1) & ~res);
                          /* axxxbxxxcxxxdxxxexxxfxxxgxxxhxxx */
    carry >>= 3;          /* 000axxxbxxxcxxxdxxxexxxfxxxgxxxh */
    carry &= 0x11111111;  /* 000a000b000c000d000e000f000g000h */
    carry |= carry >> 3;  /* 000a00ab00bc00cd00de00ef00fg00gh */
    carry &= 0x03030303;  /* 000000ab000000cd000000ef000000gh */
    carry |= carry >> 6;  /* 000000ab0000abcd000000ef0000efgh */
    carry &= 0x000f000f;  /* 000000000000abcd000000000000efgh */
    carry |= carry >> 12; /* 000000000000abcd00000000abcdefgh */
    carry &= 0x000000ff;  /* 000000000000000000000000abcdefgh */

    env->psw &= ~PSW_CB;
    env->psw |= carry << PSW_CB_SHIFT;

    return res;
}

/* TODO */
/* gen_op_add_T1_imm(1) -- for ADDC and ADDCO */
/* overflow trap for ADDO */
/* set condition flags */
/* set nullify if condition met */

#if 0
void OPPROTO op_comclr_T1_T0(void)
{
    /* T0 - T1; */
    T0 = 0;
}

void OPPROTO op_next_insn(void)
{
    env->iaoq[0] = env->iaoq[1];
    env->iasq[0] = env->iasq[1];
    env->iaoq[1] += 4;
}

void OPPROTO op_addit_T0(void)
{
    /* XXX */
}

void OPPROTO op_addito_T0(void)
{
    /* XXX */
}

void OPPROTO op_undef_insn(void)
{
    /* undefined; let's kill the process so that we can easily identify
     * any incorrect insn decoding
     */
    int *p = NULL;
    T0 = *p;
}

void OPPROTO op_ill_insn(void)
{
    raise_exception(EXCP_ILLEGAL);
}

void OPPROTO op_sub_T1_T0_cc(void)
{
/* TODO: lazy PSW */
    target_ulong borrow;
    target_ulong tmp;

    tmp = T0;
    T0 -= T1;

    /* calculate carry/borrow flags */
    borrow = (~tmp & T1) | (~(tmp ^ T1) & T0);
                            /* axxxbxxxcxxxdxxxexxxfxxxgxxxhxxx */
    borrow >>= 3;           /* 000axxxbxxxcxxxdxxxexxxfxxxgxxxh */
    borrow &= 0x11111111;   /* 000a000b000c000d000e000f000g000h */
    borrow |= borrow >> 3;  /* 000a00ab00bc00cd00de00ef00fg00gh */
    borrow &= 0x03030303;   /* 000000ab000000cd000000ef000000gh */
    borrow |= borrow >> 6;  /* 000000ab0000abcd000000ef0000efgh */
    borrow &= 0x000f000f;   /* 000000000000abcd000000000000efgh */
    borrow |= borrow >> 12; /* 000000000000abcd00000000abcdefgh */
    borrow &= 0x000000ff;   /* 000000000000000000000000abcdefgh */

    env->psw &= ~PSW_CB;
    env->psw |= ~borrow << PSW_CB_SHIFT;
}

void OPPROTO op_subb_T1_T0_cc(void)
{
/* TODO: lazy PSW */
    target_ulong borrow;
    target_ulong tmp;

    tmp = T0;
    T0 -= T1;

    /* subtract the borrow */
    if (!(env->psw & PSW_CB7)) {
        T0 -= 1;
    }

    /* calculate carry/borrow flags */
    borrow = (~tmp & T1) | (~(tmp ^ T1) & T0);
                            /* axxxbxxxcxxxdxxxexxxfxxxgxxxhxxx */
    borrow >>= 3;           /* 000axxxbxxxcxxxdxxxexxxfxxxgxxxh */
    borrow &= 0x11111111;   /* 000a000b000c000d000e000f000g000h */
    borrow |= borrow >> 3;  /* 000a00ab00bc00cd00de00ef00fg00gh */
    borrow &= 0x03030303;   /* 000000ab000000cd000000ef000000gh */
    borrow |= borrow >> 6;  /* 000000ab0000abcd000000ef0000efgh */
    borrow &= 0x000f000f;   /* 000000000000abcd000000000000efgh */
    borrow |= borrow >> 12; /* 000000000000abcd00000000abcdefgh */
    borrow &= 0x000000ff;   /* 000000000000000000000000abcdefgh */

    env->psw &= ~PSW_CB;
    env->psw |= ~borrow << PSW_CB_SHIFT;
}

void OPPROTO op_ds_T1_T0(void)
{
    /* XXX */
}

void OPPROTO op_uxor_T1_T0(void)
{
    /* XXX */
}

void OPPROTO op_uaddcm_T1_T0(void)
{
    /* XXX */
    /* store carry in T2 */
}

void OPPROTO op_dcor_T0(void)
{
    T0 -= ((((env->psw & PSW_CB7) ? 0 : 6) << 4) |
          ((((env->psw & PSW_CB6) ? 0 : 6) << 4) |
          ((((env->psw & PSW_CB5) ? 0 : 6) << 4) |
          ((((env->psw & PSW_CB4) ? 0 : 6) << 4) |
          ((((env->psw & PSW_CB3) ? 0 : 6) << 4) |
          ((((env->psw & PSW_CB2) ? 0 : 6) << 4) |
          ((((env->psw & PSW_CB1) ? 0 : 6) << 4) |
          ((((env->psw & PSW_CB0) ? 0 : 6))))))))));
}

void OPPROTO op_idcor_T0(void)
{
    T0 += ((((env->psw & PSW_CB7) ? 6 : 0) << 4) |
          ((((env->psw & PSW_CB6) ? 6 : 0) << 4) |
          ((((env->psw & PSW_CB5) ? 6 : 0) << 4) |
          ((((env->psw & PSW_CB4) ? 6 : 0) << 4) |
          ((((env->psw & PSW_CB3) ? 6 : 0) << 4) |
          ((((env->psw & PSW_CB2) ? 6 : 0) << 4) |
          ((((env->psw & PSW_CB1) ? 6 : 0) << 4) |
          ((((env->psw & PSW_CB0) ? 6 : 0))))))))));
}

/* Shift/deposit insns */
void OPPROTO op_shrpw_cc(void)
{
    /* INPUT: T0 = shift amount, T1 and T2 = register pair values */
    /* OUTPUT: T0 */
    T0 &= 0x1f;

    if (T0 == 0)
        T0 = T2; /* avoid "<< 32" with undefined result */
    else
        T0 = (T1 << (32 - T0)) | (T2 >> T0);
    FORCE_RET();
}

void OPPROTO op_extrw_cc(void)
{
    /* INPUT: T0 = shift pos, T1 = shift len, T2 = in value */
    /* OUTPUT: T0 */
}

void OPPROTO op_depw_cc(void)
{
    /* INPUT: T0 = shift pos, T1 = shift len, T2 = in value */
    /* OUTPUT: T0 */
}

void OPPROTO op_save_pc(void)
{
    env->iaoq[0] = PARAM1;
    env->iaoq[1] = PARAM2;
}

/* Compare operations */

#define signed_overflow_add(op1, op2, res) \
    (!!(((op1 ^ op2 ^ ~0) & (op1 ^ res)) >> 31))

#define signed_overflow_sub(op1, op2, res) \
    (!!(((op1 ^ op2) & (op1 ^ res)) >> 31))

void OPPROTO op_eval_never(void)
{
    T2 = 0;
}

void OPPROTO op_eval_always(void)
{
    T2 = 1;
}

/* Addition conditions:
 * See table 5-4 in PA1.1 Specification
 */

/* = */
void OPPROTO op_eval_add_eq(void)
{
    target_ulong res = T0 + T1;
    T2 = (res == 0);
}

/* < */
void OPPROTO op_eval_add_slt(void)
{
    target_long res = T0 + T1;
    T2 = ((res < 0) != signed_overflow_add(T0, T1, res));
}

/* <= */
void OPPROTO op_eval_add_slteq(void)
{
    target_long res = T0 + T1;
    T2 = ((res == 0) || ((res < 0) != signed_overflow_add(T0, T1, res)));
}

/* nuv */
void OPPROTO op_eval_add_nuv(void)
{
    target_ulong res = T0 + T1;
    T2 = (res >= T0);
}

/* znv */
void OPPROTO op_eval_add_znv(void)
{
    target_ulong res = T0 + T1;
    T2 = ((res == 0) || (res >= T0));
}

/* sv */
void OPPROTO op_eval_add_sv(void)
{
    target_long res = T0 + T1;
    T2 = signed_overflow_add(T0, T1, res);
}

/* od */
void OPPROTO op_eval_add_od(void)
{
    target_ulong res = T0 + T1;
    T2 = (res & 1);
}

/* Addition conditions with carry */

/* = */
void OPPROTO op_eval_addc_eq(void)
{
    target_ulong res = T0 + T1 + !!(env->psw & PSW_CB7);
    T2 = (res == 0);
}

/* < */
void OPPROTO op_eval_addc_slt(void)
{
    target_long res = T0 + T1 + !!(env->psw & PSW_CB7);
    T2 = ((res < 0) != signed_overflow_add(T0, T1, res));
}

/* <= */
void OPPROTO op_eval_addc_slteq(void)
{
    target_long res = T0 + T1 + !!(env->psw & PSW_CB7);
    T2 = ((res == 0) || ((res < 0) != signed_overflow_add(T0, T1, res)));
}

/* nuv */
void OPPROTO op_eval_addc_nuv(void)
{
    target_ulong res = T0 + T1 + !!(env->psw & PSW_CB7);
    if (env->psw & PSW_CB7)
        T2 = (res > T0);
    else
        T2 = (res >= T0);
    FORCE_RET();
}

/* znv */
void OPPROTO op_eval_addc_znv(void)
{
    target_ulong res = T0 + T1 + !!(env->psw & PSW_CB7);
    if (env->psw & PSW_CB7)
        T2 = ((res == 0) || (res > T0));
    else
        T2 = ((res == 0) || (res >= T0));
    FORCE_RET();
}

/* sv */
void OPPROTO op_eval_addc_sv(void)
{
    target_long res = T0 + T1 + !!(env->psw & PSW_CB7);
    T2 = signed_overflow_add(T0, T1, res);
}

/* od */
void OPPROTO op_eval_addc_od(void)
{
    target_ulong res = T0 + T1 + !!(env->psw & PSW_CB7);
    T2 = (res & 1);
}

/* Compare/subtract conditions:
 * See table 5-3 in PA1.1 Specification
 */

/* = */
void OPPROTO op_eval_sub_eq(void)
{
    target_ulong res = T0 - T1;
    T2 = (res == 0);
}

/* < */
void OPPROTO op_eval_sub_slt(void)
{
    target_long res = T0 - T1;
    T2 = ((res < 0) != signed_overflow_sub(T0, T1, res));
}

/* <= */
void OPPROTO op_eval_sub_slteq(void)
{
    target_long res = T0 - T1;
    T2 = ((res == 0) || ((res < 0) != signed_overflow_sub(T0, T1, res)));
}

/* << */
void OPPROTO op_eval_sub_ult(void)
{
    target_ulong res = T0 - T1;
    T2 = (res < 0);
}

/* <<= */
void OPPROTO op_eval_sub_ulteq(void)
{
    target_ulong res = T0 - T1;
    T2 = ((res == 0) || (res < T0));
}

/* sv */
void OPPROTO op_eval_sub_sv(void)
{
    target_long res = T0 - T1;
    T2 = signed_overflow_sub(T0, T1, res);
}

/* od */
void OPPROTO op_eval_sub_od(void)
{
    target_long res = T0 - T1;
    T2 = (res & 1);
}

/* Compare operations with borrow */

/* = */
void OPPROTO op_eval_subb_eq(void)
{
    target_ulong res = T0 - T1 - !(env->psw & PSW_CB7);
    T2 = (res == 0);
}

/* < */
void OPPROTO op_eval_subb_slt(void)
{
    target_long res = T0 - T1 - !(env->psw & PSW_CB7);
    T2 = ((res < 0) != signed_overflow_sub(T0, T1, res));
}

/* <= */
void OPPROTO op_eval_subb_slteq(void)
{
    target_long res = T0 - T1 - !(env->psw & PSW_CB7);
    T2 = ((res == 0) || ((res < 0) != signed_overflow_sub(T0, T1, res)));
}

/* << */
void OPPROTO op_eval_subb_ult(void)
{
    target_ulong res = T0 - T1 - !(env->psw & PSW_CB7);
    T2 = (res < 0);
}

/* <<= */
void OPPROTO op_eval_subb_ulteq(void)
{
    target_ulong res = T0 - T1 - !(env->psw & PSW_CB7);
    T2 = ((res == 0) || (res < T0));
}

/* sv */
void OPPROTO op_eval_subb_sv(void)
{
    target_long res = T0 - T1 - !(env->psw & PSW_CB7);
    T2 = signed_overflow_sub(T0, T1, res);
}

/* od */
void OPPROTO op_eval_subb_od(void)
{
    target_long res = T0 - T1 - !(env->psw & PSW_CB7);
    T2 = (res & 1);
}

/* Logical conditions:
 * See table 5-5 in PA1.1 Specification
 */

/* = */
void OPPROTO op_eval_log_eq(void)
{
    T2 = (T0 == 0);
}

/* < */
void OPPROTO op_eval_log_slt(void)
{
    T2 = ((target_long)T0 < 0);
}

/* <= */
void OPPROTO op_eval_log_slteq(void)
{
    T2 = ((target_long)T1 <= 0);
}

/* od */
void OPPROTO op_eval_log_od(void)
{
    T2 = (T0 & 1);
}

/* Unit conditions:
 * See table 5-6 in PA1.1 Specification
 */

void OPPROTO op_eval_unit_sbz(void)
{
    T2 = ((T0 - 0x01010101) & ~T0 & 0x80808080);
}

void OPPROTO op_eval_unit_shz(void)
{
    T2 = ((T0 - 0x00010001) & ~T0 & 0x80008000);
}

void OPPROTO op_eval_unit_sdc(void)
{
    T2 &= 0x88888888;
}

void OPPROTO op_eval_unit_sbc(void)
{
    T2 &= 0x80808080;
}

void OPPROTO op_eval_unit_shc(void)
{
    T2 &= 0x80008000;
}

/* Shift/extract/deposit conditions
 * See table 5-7 in PA1.1 Specification
 */

/* <> */
void OPPROTO op_eval_log_neq(void)
{
    T2 = (T0 != 0);
}

/* >= */
void OPPROTO op_eval_log_sgteq(void)
{
    T2 = ((target_long)T0 >= 0);
}

/* ev */
void OPPROTO op_eval_log_ev(void)
{
    T2 = !(T0 & 1);
}

/* ------ */

void OPPROTO op_jmp_im(void)
{
    env->iaoq[0] = (uint32_t)PARAM1;
    env->iaoq[1] = env->iaoq[0] + 4;
}

void OPPROTO op_jnz_T2_label(void)
{
    if (T2)
        GOTO_LABEL_PARAM(1);
    FORCE_RET();
}

void OPPROTO op_jz_T2_label(void)
{
    if (!T2)
        GOTO_LABEL_PARAM(1);
    FORCE_RET();
}
#endif
