/*
 *  HPPA virtual CPU header
 *
 *  Copyright (c) 2005-2009 Stuart Brady <stuart.brady@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA  02110-1301 USA
 */

#ifndef CPU_HPPA_H
#define CPU_HPPA_H

#include "config.h"

#if !defined(TARGET_HPPA64)
#define TARGET_LONG_BITS 32
#else
#define TARGET_LONG_BITS 64
#endif

#define CPUState struct CPUHPPAState

#include "cpu-defs.h"

#include <setjmp.h>

#include "softfloat.h"

#define TARGET_HAS_ICE

#define ELF_MACHINE	EM_PARISC

/* Group 1 interruptions */
#define EXCP_HPMC	1	/* High-priority machine check */
/* Group 2 interruptions */
#define EXCP_POWERFAIL	2
#define EXCP_RCVRCTR	3
#define EXCP_EXTINT	4
#define EXCP_LPMC	5
#define EXCP_PERFMON	29
/* Group 3 interruptions */
#define EXCP_ITLBMISS	6
#define EXCP_IMEMTRAP	7
#define EXCP_ILLEGAL	8
#define EXCP_BREAK	9
#define EXCP_PRIVOP	10
#define EXCP_PRIVREG	11
#define EXCP_OVERFLOW	12
#define EXCP_CONDITIONAL 13
#define EXCP_ASSISTEX	14
#define EXCP_DTLBMISS	15
#define EXCP_NAITLBMISS	16
#define EXCP_NADTLBMISS	17
#define EXCP_DMEMACCESS	26
#define EXCP_DMEMPROT	27
#define EXCP_UNALIGNED	28
#define EXCP_DMEMPROT2	19
#define EXCP_TLBDIRTY	20
#define EXCP_PAGEREF	21
#define EXCP_ASSISTEMU	22
/* Group 4 interruptions */
#define EXCP_HIPRIVXFER	23
#define EXCP_LOPRIVXFER	24
#define EXCP_TAKENBR	25

/* the shadow registers map to the following general registers */
// int shrmap[] = { 1, 6, 9, 16, 17, 24, 25 };

#define PSW_I   0x00000001
#define PSW_D   0x00000002
#define PSW_P   0x00000004
#define PSW_Q   0x00000008
#define PSW_R   0x00000010
#define PSW_F   0x00000020
#define PSW_G   0x00000040
#define PSW_O   0x00000080
#define PSW_CB  0x0000ff00
#define PSW_CB0 0x00000100
#define PSW_CB1 0x00000200
#define PSW_CB2 0x00000400
#define PSW_CB3 0x00000800
#define PSW_CB4 0x00001000
#define PSW_CB5 0x00002000
#define PSW_CB6 0x00004000
#define PSW_CB7 0x00008000
#define PSW_M   0x00010000
#define PSW_V   0x00020000
#define PSW_C   0x00040000
#define PSW_B   0x00080000
#define PSW_X   0x00100000
#define PSW_N   0x00200000
#define PSW_L   0x00400000
#define PSW_H   0x00800000
#define PSW_T   0x01000000
#define PSW_S   0x02000000
#define PSW_E   0x04000000
#define PSW_W   0x08000000
#define PSW_Z   0x40000000
#define PSW_Y   0x80000000

#define PSW_CB_SHIFT    8

#define NB_MMU_MODES 2

typedef struct CPUHPPAState {
    target_ulong gr[32];  /* General Registers */
    target_ulong shr[7];  /* SHadow Registers */
    uint32_t sr[8];       /* Space Registers */
    uint32_t cr[32];      /* Control Registers */
#if 0
    uint32_t cpr[n][8];   /* Co-Processor Registers */
#endif
    uint32_t fpr[32];     /* Floating-Point Registers */

    uint32_t psw;         /* Processor Status Word */
    target_ulong iaoq[2]; /* Instruction Address Offset Queue */
    uint32_t iasq[2];     /* Instruction Address Space Queue */
    target_ulong iiaoq_back;
    uint32_t iiasq_back;

    uint8_t priv_level;

 /*  gr[0]  : permanently 0
  *  gr[1]  : target for ADDIL
  *  gr[31] : link register for BLE
  */

    int interrupt_index;

    CPU_COMMON

    /* Fields after the common ones are preserved on reset.  */
    struct hppa_boot_info *boot_info;
} CPUHPPAState;

#define TARGET_PAGE_BITS 12

#define cpu_init cpu_hppa_init
#define cpu_exec cpu_hppa_exec
#define cpu_gen_code cpu_hppa_gen_code
#define cpu_signal_handler cpu_hppa_signal_handler

/* MMU modes definitions */
#define MMU_MODE0_SUFFIX _kernel
#define MMU_MODE1_SUFFIX _user
#define MMU_USER_IDX 0
static inline int cpu_mmu_index (CPUState *env)
{
#if defined(CONFIG_USER_ONLY)
    return 0;
#else
    return (env->iaoq[0] & 3) != 0 ? 1 : 0;
#endif
}

#if defined(CONFIG_USER_ONLY)
static inline void cpu_clone_regs(CPUState *env, target_ulong newsp)
{
    if (newsp)
        env->gr[30] = newsp;
    /* XXXXX */
}
#endif

#include "cpu-all.h"
#include "exec-all.h"

CPUHPPAState *cpu_hppa_init(const char *cpu_model);
void hppa_translate_init(void);
int cpu_hppa_exec(CPUHPPAState *s);
int cpu_hppa_signal_handler(int host_signum, void *pinfo,
                            void *puc);
int cpu_hppa_handle_mmu_fault(CPUHPPAState *env, target_ulong address, int rw,
                              int mmu_idx, int is_softmmu);
void do_interrupt(CPUHPPAState *env);

void hppa_cpu_list(FILE *f, int (*cpu_fprintf)(FILE *f, const char *fmt, ...));

static inline void cpu_pc_from_tb(CPUState *env, TranslationBlock *tb)
{
    env->iaoq[0] = tb->pc;
    env->iaoq[1] = tb->cs_base;
}

static inline void cpu_get_tb_cpu_state(CPUState *env, target_ulong *pc,
                                        target_ulong *cs_base, int *flags)
{
    *flags = env->psw & PSW_N; /* XXX: use more bits? */
    *pc = env->iaoq[0];
    *cs_base = env->iaoq[1];
}

#endif
