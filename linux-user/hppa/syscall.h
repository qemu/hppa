struct target_pt_regs {
	target_ulong gr[32];	/* PSW is in gr[0] */
	target_ulong sr[ 8];
	target_ulong iasq[2];
	target_ulong iaoq[2];
};

#define UNAME_MACHINE "parisc"
